# particules
Déplacement de particules en 2D réalisé à 2.


Attention
Risque epileptique potentiel avec color:4 et surtout color:5




Color :

0 : particule noir.
1 : particule blanche.
2 : couleur aleatoire.
3 : degraderde couleur.
4 : clignottement bleu rouge depend du ColorRate  (Color rate plus grand=plus de temps entre deux changement de couleur).
5 : scintillement aleatoire noir blanc depend du ColorRate.
donne par ex un resultat interessant avec : initnbparticule a 50, spawnrate 0, addvitesse fals,e random spawn true, colorrate 10


feu artifice :

Si Fixe : la particule centrale de chaque generateur n'est plus mise a jour ni affiché les positions de genration ne change pas
Sinon : les generateurs se deplace

le feu d'artifice est géré par le init nb particule pour le nombre de fuséee(de position de générateur)
et par le  artispawnrate pour le nombre de particule par generateur

Peu intéresant avec randomspawn false

Rebond : 
Si true les particules ne sorte plus de l'écran et rebondissent sur les murs

Gravité : 
si true la valeur de gravité_val est ajouté à la vitesse à chaque update

Decrease opacity :
Si true la valeur de decrease_amount est soustraite à l'opacité (si l'opacité est a 0 la particules meurt)

Duree de vie :
Si true a chaque passage dans update la vie de la particule augmente de 1
si la vie depasse Duree de vie la particule n'est plus affiché ni mise a jour


Sortie de l'ecran :

Si true si la particule n'est plus dans l'écran + WindowsGap la particule n'est plus mise a jour ni affiché
