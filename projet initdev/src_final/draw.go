package main

import (
	"fmt"
	"project-particles/assets"
	"project-particles/config"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

// Draw se charge d'afficher à l'écran l'état actuel du système de particules
// g.system. Elle est appelée automatiquement environ 60 fois par seconde par
// la bibliothèque Ebiten. Cette fonction pourra être légèrement modifiée quand
// c'est précisé dans le sujet.
func (g *game) Draw(screen *ebiten.Image) {

	for _, p := range g.system.Content {
		options := ebiten.DrawImageOptions{}
		options.GeoM.Rotate(p.Rotation)
		options.GeoM.Scale(p.ScaleX, p.ScaleY)
		options.GeoM.Translate(p.PositionX, p.PositionY)
		options.ColorM.Scale(p.ColorRed, p.ColorGreen, p.ColorBlue, p.Opacity)

		if config.General.Gestion_vie || config.General.Decrese_opasity || config.General.Artifice || config.General.Gestion_sortie {
			if p.Vie < config.General.Duree_vie {
				screen.DrawImage(assets.ParticleImage, &options)
			}
		} else {
			screen.DrawImage(assets.ParticleImage, &options)
		}
	}

	if config.General.Debug {
		//ebitenutil.DebugPrint(screen, fmt.Sprint(ebiten.CurrentTPS()))

		ebitenutil.DebugPrint(screen, fmt.Sprint("nb particules: ", len(g.system.Content), " fps: ", ebiten.CurrentTPS()))
	}

}
