package config

// Config définit les champs qu'on peut trouver dans un fichier de config.
// Dans le fichier les champs doivent porter le même nom que dans le type si
// dessous, y compris les majuscules. Tous les champs doivent obligatoirement
// commencer par des majuscules, sinon il ne sera pas possible de récupérer
// leurs valeurs depuis le fichier de config.
// Vous pouvez ajouter des champs et ils seront automatiquement lus dans le
// fichier de config. Vous devrez le faire plusieurs fois durant le projet.
type Config struct {
	WindowTitle              string
	WindowSizeX, WindowSizeY int
	ParticleImage            string
	Debug                    bool
	InitNumParticles         int
	RandomSpawn              bool
	SpawnX, SpawnY           int
	SpawnRate                float64
	Addvitesse               bool
	Rebond                   bool
	Color                    int
	ColorRate                int
	Gestion_vie              bool
	Duree_vie                float64
	Gravitee                 bool
	Gravitee_val             float64
	Decrese_opasity          bool
	Decrese_amount           float64
	Gestion_sortie           bool
	WindowsGap               int
	Artifice                 bool
	ArtiSpawnRate            float64
	Fixe                     bool
}

var General Config
