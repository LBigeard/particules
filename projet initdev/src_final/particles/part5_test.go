package particles

import (
	"project-particles/config"
	"testing"
)

func Test_color01(t *testing.T) { //test color noir
	config.Get("../config.json")
	config.General.InitNumParticles = 10
	config.General.SpawnRate = 0
	config.General.Color = 0

	A := NewSystem()

	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i]
		if test.ColorRed != 0 || test.ColorBlue != 0 || test.ColorGreen != 0 {
			t.Fail()
		}
	}

	config.Get("../config.json") // avec generation de particule et mise a jour   cette fois color blanche
	config.General.InitNumParticles = 0
	config.General.SpawnRate = 2
	config.General.Color = 1

	A = NewSystem()

	for i := 0.0; i < 100; i++ {
		A.Update()
		for i := 0; i < len(A.Content); i++ {
			var test Particle = A.Content[i]
			if test.ColorRed != 1 || test.ColorBlue != 1 || test.ColorGreen != 1 {
				t.Fail()
			}
		}

	}

}

//color 2 depend de l'aleatoire et est donc difficilement testable

func Test_color3(t *testing.T) { //test si la couleur du degradé est bien configuré dés le début
	config.Get("../config.json")
	config.General.InitNumParticles = 10
	config.General.SpawnRate = 0
	config.General.Color = 3

	A := NewSystem()

	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i]
		if test.ColorRed != (test.PositionX/float64(config.General.WindowSizeX)) || test.ColorBlue != 0 || test.ColorGreen != (1-(test.PositionX/float64(config.General.WindowSizeX))) {
			t.Fail()
		}

	}

	config.General.InitNumParticles = 0 // avec generation de particule et mise a jour
	config.General.SpawnRate = 2
	config.General.Color = 3

	A = NewSystem()

	for i := 0.0; i < 100; i++ {
		A.Update()
		for i := 0; i < len(A.Content); i++ {
			var test Particle = A.Content[i]
			if test.ColorRed != (test.PositionX/float64(config.General.WindowSizeX)) || test.ColorBlue != 0 || test.ColorGreen != (1-(test.PositionX/float64(config.General.WindowSizeX))) {
				t.Fail()
			}

		}

	}

	config.General.InitNumParticles = 1 //verifie pour 2 position differentes si les couleurs sont bien differentes
	config.General.SpawnRate = 0
	config.General.Color = 3
	config.General.SpawnX = 200
	config.General.RandomSpawn = false

	A = NewSystem()

	var test Particle = A.Content[0]

	config.General.InitNumParticles = 1
	config.General.SpawnRate = 0
	config.General.Color = 3
	config.General.SpawnX = 400
	config.General.RandomSpawn = false

	B := NewSystem() //on test avec un nouveau systeme

	var test1 Particle = B.Content[0]

	if test.ColorRed == test1.ColorRed {
		t.Error("les couleur devrait etre differentes")
	}

}

func Test_color4(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 100
	config.General.SpawnRate = 0
	config.General.Color = 4
	config.General.ColorRate = 4
	config.General.Gestion_sortie = false
	A := NewSystem()

	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i]
		if test.ColorRed != 1 || test.ColorBlue != 0 || test.ColorGreen != 0 {
			t.Fail()
		}

	}

	for j := 1; j < config.General.InitNumParticles; j++ {
		A.Update()

		var test Particle = A.Content[j]

		if j > config.General.ColorRate { //pour eviter un panic erreur car pas assez de modiff avant
			if A.Content[j-config.General.ColorRate].ColorRed == 1 { //si la couleur colorate(ici 4) fois avant etait rouge
				if test.ColorRed != 1 || test.ColorBlue != 0 || test.ColorGreen != 0 { //il faut que sa redevienne rouge
					t.Error("couleur n'est pas rouge quand il le faudrait")

				}
				//fmt.Println((j%config.General.ColorRate),j,test.ColorRed)
			} else {
				if test.ColorRed != 0 || test.ColorBlue != 1 || test.ColorGreen != 0 { //il faut que sa redevienne bleu
					t.Error("couleur n'est pas bleu quand il le faudrait")

				}
			}

		}

	}

	config.General.InitNumParticles = 0
	config.General.SpawnRate = 3
	config.General.Color = 4
	config.General.ColorRate = 3

	A = NewSystem()

	for j := 1; j < 10; j++ {
		A.Update()

		for i := 0; i < len(A.Content); i++ {

			var test Particle = A.Content[j]

			if j > config.General.ColorRate { //pour eviter un panic erreur car pas assez de modiff avant
				if A.Content[j-config.General.ColorRate].ColorRed == 1 { //si la couleur colorate(ici 4) fois avant etait rouge
					//fmt.Println(A.Content[j-config.General.ColorRate])
					if test.ColorRed != 1 || test.ColorBlue != 0 || test.ColorGreen != 0 { //il faut que sa redevienne rouge
						t.Error("couleur n'est pas rouge quand il le faudrait")

					}

				} else {
					if test.ColorRed != 0 || test.ColorBlue != 1 || test.ColorGreen != 0 { //il faut que sa redevienne bleu
						t.Error("couleur n'est pas bleu quand il le faudrait")

					}
				}

			}
			//fmt.Println((j % config.General.ColorRate), j, test.ColorRed)
			//fmt.Println(A.Content)
		}

	}

}

//color 5 depend de l'aleatoire et est donc difficilement testable

func Test_dureevie(t *testing.T) {
	config.Get("../config.json")
	config.General.InitNumParticles = 100
	config.General.SpawnRate = 0
	config.General.Gestion_vie = true
	config.General.Duree_vie = 500
	config.General.Decrese_opasity = false
	config.General.Artifice = false
	config.General.Gestion_sortie = false

	A := NewSystem()

	for j := 0; j < 354; j++ {
		A.Update()

	}
	for i := 0; i < len(A.Content); i++ {

		var test Particle = A.Content[i]
		if test.Vie != 354 || test.Vie > config.General.Duree_vie {
			t.Error("la duree de vie devrait etre a", 354, "mais elle est a ", test.Vie)
		}
	}

	config.General.InitNumParticles = 100 //verif si la vie s'arrete bien une fois au max
	config.General.SpawnRate = 0
	config.General.Gestion_vie = true
	config.General.Duree_vie = 500
	config.General.Artifice = false
	config.General.Decrese_opasity = false

	A = NewSystem()

	for j := 0; j < 600; j++ {
		A.Update()

	}
	for i := 0; i < len(A.Content); i++ {

		var test Particle = A.Content[i]
		if test.Vie != config.General.Duree_vie || test.Vie > config.General.Duree_vie {
			t.Error("la duree de vie devrait etre a", config.General.Duree_vie, "mais elle est a ", test.Vie)
		}
	}

	config.General.InitNumParticles = 0
	config.General.SpawnRate = 5
	config.General.Decrese_opasity = false
	config.General.Artifice = false

	A = NewSystem()

	for j := 0.0; j < 600; j++ {
		A.Update()
		var test Particle = A.Content[len(A.Content)-1]

		if test.Vie != 1 || test.Vie > config.General.Duree_vie {
			t.Error("la duree de vie devrait etre a", 1, "mais elle est a ", test.Vie)
		}
	}

	config.General.SpawnRate = 1

	A = NewSystem()

	for j := 0.0; j < 2; j++ {
		A.Update()

		for i := 0; i < len(A.Content); i++ {

			var test Particle = A.Content[0]
			if test.Vie != j+1 || test.Vie > config.General.Duree_vie {
				t.Error("la duree de vie devrait etre a", j+1, "mais elle est a ", test.Vie)
			}
		}
	}

	config.General.InitNumParticles = 1
	config.General.SpawnRate = 0
	config.General.Gestion_vie = true
	config.General.Duree_vie = 10
	config.General.Addvitesse = true
	config.General.Artifice = false
	config.General.Decrese_opasity = false

	A = NewSystem()
	var test Particle = A.Content[0]
	var posX = test.PositionX

	for j := 0; j < 5; j++ {
		A.Update()

	}
	var test2 Particle = A.Content[0]
	var posXafter = test2.PositionX

	if posX == posXafter { //la position doit avoir changer
		t.Error("la position na pas changer")
	}

	for j := 0; j < 10; j++ { //depassement de la duree de vie
		A.Update()

	}

	var newtest Particle = A.Content[0]
	var newposX = newtest.PositionX

	for j := 0; j < 5; j++ {
		A.Update()

	}
	var newtest2 Particle = A.Content[0]
	var newposXafter = newtest2.PositionX

	if newposX != newposXafter { // la position ne doit plus changer
		t.Error("la position a changer", newposX, newposXafter)
	}

}

func Test_Artifice(t *testing.T) {
	config.Get("../config.json") //verifie si les particule de base sont bien fixe(vie superieur a duree de vie) et donc sont bien parametré pour les generateurs
	config.General.InitNumParticles = 100
	config.General.SpawnRate = 0
	config.General.Gestion_vie = false
	config.General.Duree_vie = 500
	config.General.Decrese_opasity = false
	config.General.Artifice = true
	config.General.ArtiSpawnRate = 0
	config.General.Gestion_sortie = false
	config.General.Fixe = true
	A := NewSystem()

	A.Update()

	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i]
		if test.Vie != 501 { //500 car dureee de vie +1 pour que sa depasse
			t.Error(test.Vie)
		}
	}

	config.General.InitNumParticles = 10 //verifie qu'il y a le bon nombre de paricule
	config.General.SpawnRate = 0
	config.General.Decrese_opasity = false
	config.General.Artifice = true
	config.General.ArtiSpawnRate = 2
	config.General.Fixe = false

	A = NewSystem()

	for i := 0; i < 10; i++ {
		A.Update()
	}

	if len(A.Content) != (10*10)*2+10 { //il doit y avoir nombre d'update(10)*particule a la base(10)*Artispawnrate(2)+10 particule a la base

		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", 10*float64(config.General.InitNumParticles)*config.General.ArtiSpawnRate+float64(config.General.InitNumParticles))
	}

	config.General.InitNumParticles = 42
	config.General.SpawnRate = 0
	config.General.Decrese_opasity = false
	config.General.Artifice = true
	config.General.ArtiSpawnRate = 0.002
	config.General.Fixe = false

	A = NewSystem()

	for i := 0; i < 10000; i++ { //il faut un nombre d'update suffisament grand avec un artispawnrate aussi faible
		A.Update()
	}

	if float64(len(A.Content)) != 10000*float64(config.General.InitNumParticles)*config.General.ArtiSpawnRate+float64(config.General.InitNumParticles) { //il doit y avoir 10 particule a la base + nombre d'update(10)*particule a la base(10)*Artispawnrate(2)

		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", 10000*float64(config.General.InitNumParticles)*config.General.ArtiSpawnRate+float64(config.General.InitNumParticles))
	}

	config.General.InitNumParticles = 10 //on verifie si artifice ne passe pas les mauvaises particules au dessus de duree de vie
	config.General.SpawnRate = 0
	config.General.Gestion_vie = true //on aurait pu faire avec false aussi
	config.General.Duree_vie = 500
	config.General.Decrese_opasity = false
	config.General.Artifice = true
	config.General.ArtiSpawnRate = 1
	config.General.Fixe = false

	A = NewSystem()

	for i := 0; i < 250; i++ {
		A.Update()
	}
	for i := 0; i < len(A.Content); i++ {
		if i > config.General.InitNumParticles { //uniquement les nouvelles particules
			var test Particle = A.Content[i]
			if test.Vie >= config.General.Duree_vie { // avec seulement 250 update aucune nouvelle particule ne doit avoir depasser la duree de vie elle sont donc mise a jour
				t.Error(test.Vie)

			}

		}
	}

}

func Test_Sortieecran(t *testing.T) { //verifie si les particule sont bien tué si elle sont en dehors de lecran(+gap)
	config.Get("../config.json")

	config.General.Gestion_sortie = true
	config.General.WindowsGap = -10000
	config.General.Rebond = false
	config.General.WindowSizeX = 500

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false

	config.General.Addvitesse = true

	config.General.Duree_vie = 600
	config.General.Gestion_vie = false
	config.General.SpawnRate = 0
	config.General.Fixe = true

	A := NewSystem()

	A.Update()

	A.Content[0].PositionX = float64(config.General.WindowSizeX) + float64(config.General.WindowsGap) + 1 // normalement au prochain passage dans Update la particule sera "tuée"

	A.Update()

	var positionX = A.Content[0].PositionX
	var positionY = A.Content[0].PositionY

	A.Update()
	A.Update()
	A.Update()
	A.Update()
	A.Update()

	if A.Content[0].PositionX != positionX || A.Content[0].PositionY != positionY {
		t.Error("le wndows gap ne fonctionne pas")
	}

}

func Test_reb(t *testing.T) { //vérif si le rebond est bien appliqué
	config.Get("../config.json")
	config.General.RandomSpawn = true
	config.General.InitNumParticles = 1
	config.General.SpawnX = 200
	config.General.SpawnY = 300

	/*
		config.General.WindowSizeX
		config.General.WindowSizeY
	*/
	A := NewSystem()
	A.Content[0].SpeedX = A.Content[0].SpeedX * 10
	for i := 0; i < 100; i++ { //On fait 20 appel d'update pour appliquer plusieur fois la vitesse
		A.Update()
	}
	if A.Content[0].PositionX > float64(config.General.WindowSizeX) || A.Content[0].PositionX < float64(0) || A.Content[0].PositionY > float64(config.General.WindowSizeY) || A.Content[0].PositionY < float64(0) {
		t.Error("les coordonnées de la particules sont", A.Content[0].PositionX, A.Content[0].PositionY, "elles devraient être inférieur à", config.General.WindowSizeX, config.General.WindowSizeY)
	}
}

func Test_rebaxeX(t *testing.T) { //Ici on test si la vitesse de l'axe X est bien inversé après contact avec une limite de l'axe
	config.Get("../config.json")

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Gravitee = false
	config.General.Rebond = true

	A := NewSystem()

	var vitesseX = A.Content[0].SpeedX
	var vitesseX1 = A.Content[1].SpeedX
	A.Content[0].PositionX = float64(config.General.WindowSizeX) * 2
	A.Content[1].PositionX = -200
	A.Update()

	if A.Content[0].SpeedX != vitesseX*-1 {
		t.Error("La vitesse est", A.Content[0].SpeedX, "elles devraient être égale à", vitesseX*-1)
	}
	if A.Content[1].SpeedX != vitesseX1*-1 {
		t.Error("La vitesse est", A.Content[1].SpeedX, "elles devraient être égale à", vitesseX1*-1)
	}
}

func Test_rebaxeY(t *testing.T) { //Ici on test si la vitesse de l'axe Y est bien inversé après contact avec une limite de l'axe
	config.Get("../config.json")
	config.General.Rebond = true
	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Gravitee = false

	A := NewSystem()

	var vitesseY = A.Content[0].SpeedY
	var vitesseY1 = A.Content[1].SpeedY
	A.Content[0].PositionY = float64(config.General.WindowSizeY) * 2
	A.Content[1].PositionY = -200
	A.Update()

	if A.Content[0].SpeedY != vitesseY*-1 {
		t.Error("La vitesse est", A.Content[0].SpeedY, "elles devraient être égale à", vitesseY*-1)
	}
	if A.Content[1].SpeedY != vitesseY1*-1 {
		t.Error("La vitesse est", A.Content[1].SpeedY, "elles devraient être égale à", vitesseY1*-1)
	}
}

/*-----------------Gravité-----------------*/

func Test_Gravitee(t *testing.T) { //Ici on test si la constante de gravitée est bien appliqué sur la vitesse
	config.Get("../config.json")

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Gravitee = true
	config.General.Addvitesse = true

	A := NewSystem()
	var initial_vitesse = A.Content[0].SpeedY
	A.Update()

	if A.Content[0].SpeedY != initial_vitesse+config.General.Gravitee_val {
		t.Error("La vitesse est pas bonne")
	}
}

func Test_Gravitee1(t *testing.T) { //Ici on test si la constante de gravitée est bien appliqué sur la vitesse
	config.Get("../config.json")

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Gravitee = true
	config.General.Addvitesse = true

	A := NewSystem()
	var initial_vitesse = A.Content[0].SpeedY
	A.Update()

	if A.Content[0].SpeedY != initial_vitesse+config.General.Gravitee_val {
		t.Error("La vitesse et la gravitée ne sont pas bonne")
	}
}

/*--------------Opasitée-----------------*/
func Test_Opasite(t *testing.T) { //Ici on test si l'opacité réduit bien
	config.Get("../config.json")

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Decrese_opasity = true
	config.General.Addvitesse = true
	config.General.Decrese_amount = 0.001
	config.General.Duree_vie = 600
	config.General.Gestion_vie = true

	A := NewSystem()
	var initial_opacity = A.Content[0].Opacity
	A.Update()

	if A.Content[0].Opacity != initial_opacity-config.General.Decrese_amount {
		t.Error("La reduction de l'opacity ne fonctionne pas")
	}
}

func Test_Opasite1(t *testing.T) { //Ici on test si la particule ce fait bien "tuer" après un passsage de l'opacité en dessous de 0
	config.Get("../config.json")

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Decrese_opasity = true
	config.General.Addvitesse = true
	config.General.Decrese_amount = 0.001
	config.General.Duree_vie = 600
	config.General.Gestion_vie = true

	A := NewSystem()

	A.Update()
	A.Content[0].Opacity = -1 // normalement au prochain passage dans Update la particule sera "tuée"
	A.Update()

	if A.Content[0].Vie < config.General.Duree_vie {
		t.Error("La reduction de l'opacity ne fonctionne pas")
	}
}

func Test_Killopa(t *testing.T) { //Ici on test si après le passage de l'opacité en dessous de 0 que le programme "tue" la particules et que celle ci ne réagit plus après plusieur appel d'update
	config.Get("../config.json")

	config.General.InitNumParticles = 2
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.RandomSpawn = false
	config.General.Decrese_opasity = true
	config.General.Addvitesse = true
	config.General.Decrese_amount = 0.001
	config.General.Duree_vie = 600
	config.General.Gestion_vie = false
	config.General.Artifice=false

	A := NewSystem()

	A.Update()
	A.Content[0].Opacity = -1 // normalement au prochain passage dans Update la particule sera "tuée"
	A.Update()
	var positionX = A.Content[0].PositionX
	var positionY = A.Content[0].PositionY
	A.Update()
	A.Update()
	A.Update()
	A.Update()
	A.Update()
	if A.Content[0].PositionX != positionX || A.Content[0].PositionY != positionY {
		t.Error("La reduction de l'opacity ne fonctionne pas")
	}
}
