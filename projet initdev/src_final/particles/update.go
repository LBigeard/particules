package particles

import (
	"math/rand"
	"project-particles/config"
	"time"
)

// Update mets à jour l'état du système de particules (c'est-à-dire l'état de
// chacune des particules) à chaque pas de temps. Elle est appellée exactement
// 60 fois par seconde (de manière régulière) par la fonction principale du
// projet.
// C'est à vous de développer cette fonction.

// fonctionne sur le même concept que new .go mais sans boncle for car on ne veut pas générer plussieurs particules au début mais bien une dès que c'est nécessaire

func Generateur(s *System) {
	rand.Seed(time.Now().UnixNano())
	x1 := rand.NewSource(time.Now().UnixNano())
	var random = []float64{-1.0, 1.0}

	var xu float64
	var yu float64

	if config.General.RandomSpawn {
		xu = float64(rand.Intn((config.General.WindowSizeX)))
		yu = float64(rand.Intn((config.General.WindowSizeY)))
	}
	if !config.General.RandomSpawn {
		xu = float64((config.General.SpawnX))
		yu = float64((config.General.SpawnY))
	}

	randRed := 0.0
	randGreen := 0.0
	randBlue := 0.0

	switch config.General.Color {
	case 1:
		randRed = 1
		randGreen = 1
		randBlue = 1
	case 2:
		randRed = rand.Float64()
		randGreen = rand.Float64()
		randBlue = rand.Float64()

	case 3:
		randRed = xu / float64(config.General.WindowSizeX)
		randGreen = 1 - (xu / float64(config.General.WindowSizeX))
		randBlue = 0

	case 4:
		randRed = 1
		randGreen = 0
		randBlue = 0
	}

	var p = Particle{
		PositionX: xu,
		PositionY: yu,
		ScaleX:    1, ScaleY: 1,
		ColorRed: randRed, ColorGreen: randGreen, ColorBlue: randBlue,
		Opacity:   1,
		SpeedX:    rand.New(x1).Float64() * random[rand.Intn(len(random))],
		SpeedY:    rand.New(x1).Float64() * random[rand.Intn(len(random))],
		firstpass: true,
	}

	s.Content = append(s.Content, p) //le systeme etant initialisé par new.go on se contente de rajouté les particule dans Content
}

func (s *System) Update() {

	for s.SpawnRate >= 1 { //permet de gérer un spawnrate plus petit que 1 facilement
		Generateur(s)

		s.SpawnRate -= 1

	}
	s.SpawnRate += config.General.SpawnRate

	if config.General.Artifice {
		gestionfeuartifice(s)
	}

	if (config.General.ColorRate) != 0 && (config.General.Color) > 3 {
		gestioncolorrate(s)
	}

	for i := 0; i < len(s.Content); i++ {

		if config.General.Gestion_vie || config.General.Decrese_opasity || config.General.Artifice || config.General.Gestion_sortie {

			if s.Content[i].Vie < config.General.Duree_vie {
				if config.General.Addvitesse {
					Addv(s, i)
				}
				if config.General.Rebond {
					Rebond(s, i)
				}
				if config.General.Gestion_vie {
					s.Content[i].Vie = s.Content[i].Vie + 1
				}

				if config.General.Decrese_opasity {
					Decrese(s, i)
				}

				if config.General.Gravitee {
					gravitee(s, i)
				}

				if config.General.Artifice && config.General.Fixe {
					initfeuartifice(s, i)
				}

				switch config.General.Color {
				case 3:
					colordrapeau(s, i)
				case 4:
					if s.ColorChange == 2 {
						colorclignotebleu(s, i)

					} else if s.ColorChange == 1 {
						colorclignoterouge(s, i)

					}
				case 5:
					colorscintille(s, i)

				}
				if config.General.Gestion_sortie {
					sortieecran(s, i)
				}
			}

		} else {

			if config.General.Addvitesse {
				Addv(s, i)
			}

			if config.General.Rebond {
				Rebond(s, i)
			}
			if config.General.Gravitee {
				gravitee(s, i)
			}

			switch config.General.Color {
			case 3:
				colordrapeau(s, i)
			case 4:
				if s.ColorChange == 2 {
					colorclignotebleu(s, i)

				} else if s.ColorChange == 1 {
					colorclignoterouge(s, i)

				}
			case 5:
				colorscintille(s, i)

			}

		}

	}

}

func Rebond(s *System, i int) { // quand la particule dépasse la limite d'un des axes la vitesse de cette axe est inversé
	if s.Content[i].PositionX > float64(config.General.WindowSizeX) || s.Content[i].PositionX < float64(0) { //windows+gap   float0-gap

		s.Content[i].SpeedX = s.Content[i].SpeedX * -1

	}
	if s.Content[i].PositionY > float64(config.General.WindowSizeY) || s.Content[i].PositionY < float64(0) { //windows+gap   float0-gap

		s.Content[i].SpeedY = s.Content[i].SpeedY * -1
		if config.General.Gravitee {
			s.Content[i].SpeedY = s.Content[i].SpeedY / 2
			s.Content[i].PositionY = float64(config.General.WindowSizeY) - 10
		}

	}

}
func sortieecran(s *System, i int) { // quand la particule sort de l'écran on lui fait dépasser la valeur de la durée de vie pour qu'elle ne soit plus prise en compte dans l'update (pour la "tuer")
	if s.Content[i].PositionX > float64(config.General.WindowSizeX)+float64(config.General.WindowsGap) || s.Content[i].PositionX < float64(0)-float64(config.General.WindowsGap) { //windows+gap   float0-gap

		s.Content[i].Vie = s.Content[i].Vie + 1 + config.General.Duree_vie
	}
	if s.Content[i].PositionY > float64(config.General.WindowSizeY)+float64(config.General.WindowsGap) || s.Content[i].PositionY < float64(0)-float64(config.General.WindowsGap) { //windows+gap   float0-gap

		s.Content[i].Vie = s.Content[i].Vie + 1 + config.General.Duree_vie

	}

}

func Addv(s *System, i int) {
	//à chaque passage dans update ajoute a chaque particule sa vitesse
	s.Content[i].PositionX += s.Content[i].SpeedX
	s.Content[i].PositionY += s.Content[i].SpeedY

}

func colordrapeau(s *System, i int) {
	s.Content[i].ColorRed = s.Content[i].PositionX / float64(config.General.WindowSizeX)
	s.Content[i].ColorGreen = 1 - (s.Content[i].PositionX / float64(config.General.WindowSizeX))

}

func gestioncolorrate(s *System) {
	if s.Colorvar%(config.General.ColorRate) == 0 { //on change de couleur si color var multiple de color rate
		if s.ColorChange == 1 {
			s.ColorChange = 2 //changement
		} else if s.ColorChange == 2 {
			s.ColorChange = 1 //changement
		}
		s.Colorvar += 1

	} else {
		s.Colorvar += 1

	}
}

func colorclignotebleu(s *System, i int) {
	s.Content[i].ColorRed = 0
	s.Content[i].ColorBlue = 1
	s.Content[i].ColorGreen = 0
}
func colorclignoterouge(s *System, i int) {
	s.Content[i].ColorRed = 1
	s.Content[i].ColorBlue = 0
	s.Content[i].ColorGreen = 0
}
func colorscintille(s *System, i int) {
	var alea = []float64{0.0, 1.0}
	var color float64 = alea[rand.Intn(len(alea))]
	if s.ColorChange == 2 { //color change tout le temps donc change meme si colorchange ne bouge pas
		s.Content[i].ColorRed = color
		s.Content[i].ColorBlue = color
		s.Content[i].ColorGreen = color

	} else if s.ColorChange == 1 {

	}
}

func gravitee(s *System, i int) { //on ajoute la constante de gravitée à la vitesse
	s.Content[i].SpeedY = s.Content[i].SpeedY + config.General.Gravitee_val
}

func Decrese(s *System, i int) { //On enlève à la valeur d'opacité de la particule le montant choisi dans le config
	s.Content[i].Opacity = s.Content[i].Opacity - config.General.Decrese_amount
	if s.Content[i].Opacity < 0 {
		s.Content[i].Vie = s.Content[i].Vie + 1 + config.General.Duree_vie // cela permet de de "tuer "la particule quand celle ci à une opacitée inférieur à 0 et insi ne plus la prendre en compte dans le update
	}
}

func initfeuartifice(s *System, i int) { //la particule centrale de chaque generateur n'est plus mise a jour ni affiché
	if i < config.General.InitNumParticles {
		s.Content[i].Vie = s.Content[i].Vie + 1 + config.General.Duree_vie

	}

}

func gestionfeuartifice(s *System) {
	for s.ArtiSpawnRate >= 1 { //permet de gérer un spawnrate plus petit que 1 facilement

		for j := 0; j < config.General.InitNumParticles; j++ { //on genere des particule pour chaque particule dans initnbparticule
			Generateur(s)
			if !config.General.Fixe {
				s.Content[j].Vie = 0
			}
			s.Content[len(s.Content)-1].PositionX = s.Content[j].PositionX //on leur donne les meme caracteristique que la partcule a la base sauf que les nouvelle peuvent bouger
			s.Content[len(s.Content)-1].PositionY = s.Content[j].PositionY
			s.Content[len(s.Content)-1].ColorBlue = s.Content[j].ColorBlue
			s.Content[len(s.Content)-1].ColorRed = s.Content[j].ColorRed
			s.Content[len(s.Content)-1].ColorGreen = s.Content[j].ColorGreen

		}

		s.ArtiSpawnRate -= 1

	}
	s.ArtiSpawnRate += config.General.ArtiSpawnRate

}
