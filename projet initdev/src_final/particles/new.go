package particles

import (
	"math/rand"
	"project-particles/config"
	"time"
)

// NewSystem est une fonction qui initialise un système de particules et le
// retourne à la fonction principale du projet, qui se chargera de l'afficher.
// C'est à vous de développer cette fonction.
// Dans sa version actuelle, cette fonction affiche une particule blanche au
// centre de l'écran.

func NewSystem() System {
	rand.Seed(time.Now().UnixNano()) //genere une seed aléatoire
	var a System
	x1 := rand.NewSource(time.Now().UnixNano())
	var random = []float64{-1.0, 1.0}
	var xu float64
	var yu float64
	randRed := 0.0
	randGreen := 0.0
	randBlue := 0.0

	var Nbparticules = config.General.InitNumParticles
	var tab []Particle = []Particle{}

	for i := 1; i <= Nbparticules; i++ { //boucle qui crée autant de particule que demandé dans le fichier json
		if config.General.RandomSpawn == true { //si Randomspawn est true on aura xu et yu aléatoire
			xu = float64(rand.Intn((config.General.WindowSizeX)))
			yu = float64(rand.Intn((config.General.WindowSizeY)))
		}
		if config.General.RandomSpawn == false { // sinon ils seront défini par SpawnX et Y du fichier json
			xu = float64((config.General.SpawnX))
			yu = float64((config.General.SpawnY))
		}

		switch config.General.Color {

		case 1:
			randRed = 1
			randGreen = 1
			randBlue = 1
		case 2:
			randRed = rand.Float64()
			randGreen = rand.Float64()
			randBlue = rand.Float64()
		case 3:
			randRed = xu / float64(config.General.WindowSizeX)
			randGreen = 1 - (xu / float64(config.General.WindowSizeX))
			randBlue = 0

		case 4:
			randRed = 1
			randGreen = 0
			randBlue = 0
		}
		var p = Particle{ // création de la particule
			PositionX: xu,
			PositionY: yu,
			ScaleX:    1, ScaleY: 1,
			ColorRed: randRed, ColorGreen: randGreen, ColorBlue: randBlue,
			Opacity: 1,
			SpeedX:  rand.New(x1).Float64() * random[rand.Intn(len(random))], //génération de vitessealléatoire en x et y pour chaque particule
			SpeedY:  rand.New(x1).Float64() * random[rand.Intn(len(random))],
		}

		tab = append(tab, p)
	}

	a = System{Content: tab, SpawnRate: config.General.SpawnRate, Colorvar: 1, ColorChange: 1, ArtiSpawnRate: config.General.ArtiSpawnRate} //le spawnrate est récupéré pour avoir une variable extérieur a update égal a config.General.SpawnRate

	return a
}
