package particles

import (
	"project-particles/config"
	"testing"
)

// --------------------------------------Test de la partie 4.1--------------------------------
func Test_nbrparticule(t *testing.T) { //vérifie qu'il y a assez de particules a la création
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 10

	A := NewSystem()

	if len(A.Content) != config.General.InitNumParticles {
		t.Error("le nombre de particules est égal a ", len(A.Content), "alors qu'il devrait etre égal à 10")
	}

	config.General.InitNumParticles = 100

	A = NewSystem()

	if len(A.Content) != 100 {
		t.Error("le nombre de particules est égal a ", len(A.Content), "alors qu'il devrait etre égal à 100")
	}

	config.General.InitNumParticles = 152

	A = NewSystem()

	if len(A.Content) != 152 {
		t.Error("le nombre de particules est égal a ", len(A.Content), "alors qu'il devrait etre égal à 152")
	}

	config.General.InitNumParticles = 8956

	A = NewSystem()

	if len(A.Content) != 8956 {
		t.Error("le nombre de particules est égal a ", len(A.Content), "alors qu'il devrait etre égal à 8956")
	}
}

func Test_alea(t *testing.T) { //ce test vérifie si les particules sont placé au bon endroit sans spawn random et si elle reste bien dans la fenetre.   pour testé véritablement le random : voir test aleafacultatif si dessous
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 10
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	A := NewSystem()

	for i := 0; i < len(A.Content); i++ { //parcours chaque particule
		var test Particle = A.Content[i] //récupére la particule
		if test.PositionX != 200 || test.PositionY != 300 {
			t.Error("la taille est", test.PositionX, test.PositionY, "alors qu'elle devrait être", config.General.SpawnX, config.General.SpawnY)
		}

	}

	config.General.RandomSpawn = false
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 100
	config.General.SpawnX = 200
	config.General.SpawnY = 200

	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i]
		if test.PositionX > float64(config.General.WindowSizeX) || test.PositionY > float64(config.General.WindowSizeY) {
			t.Error("la particule n'est pas dans l'écran")

		}
	}

	config.General.RandomSpawn = true
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 100
	config.General.SpawnX = 200
	config.General.SpawnY = 200

	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i]
		if test.PositionX > float64(config.General.WindowSizeX) || test.PositionY > float64(config.General.WindowSizeY) {
			t.Error("la particule n'est pas dans l'écran")

		}
	}

}

func Test_crea(t *testing.T) { //vérif si la particule est bien dans l'écran
	config.Get("../config.json")
	config.General.RandomSpawn = true
	config.General.InitNumParticles = 0
	config.General.SpawnRate = 25
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.WindowSizeX = 1000
	config.General.WindowSizeY = 500

	A := NewSystem()
	for i := 0; i < 1; i++ {
		A.Update()
		for i := 0; i < len(A.Content); i++ {

			var test Particle = A.Content[i] //récupére la particule
			if test.PositionX > float64(config.General.WindowSizeX) || test.PositionY > float64(config.General.WindowSizeY) {
				t.Error("la position est", test.PositionX, test.PositionY, "alors qu'elle devrait être", config.General.SpawnX, config.General.SpawnY)
			}
		}
	}
}

/*            // ce test n'a pas été validé car il y a une très faible chance que qu'une particule soit à la position de SpawnX SpawnY  ils serait par contre possible de le rendre encore moins probable
func Test_facultatifalea (t *testing.T){
	config.General.RandomSpawn=true
	config.General.SpawnRate=0
	config.General.InitNumParticles=2
	config.General.SpawnX=200
	config.General.SpawnY=200
	A:=NewSystem()
	for i:=0;i<len(A.Content);i++{                     //parcours chaque particule
		var test2 Particle = A.Content[i]              //récupére la particule
		if test2.PositionX ==200 || test2.PositionY == 200 {
			t.Error("la taille est",test2.PositionX,test2.PositionY, "alors qu'elle devrait être",config.General.SpawnX,config.General.SpawnY, "mais il existe une chance microscopique que ce test ne passe pas aléatoirement, alors vous pouvez réessayer")
		}

	}
}*/
//___________________________________________________________________________________________
//--------------------------------------Test de la partie 4.2--------------------------------
//___________________________________________________________________________________________

func Test_vitesse(t *testing.T) { //vérif pour chaque particules si la position est bien modifié
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 10
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.Addvitesse = true
	A := NewSystem()
	A.Update()
	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i] //récupére la particule
		if test.PositionX != 200+test.SpeedX && test.PositionY != 200+test.SpeedY {
			t.Error("les vitesses de x et y sont de", test.SpeedX, test.SpeedY, "mais leurs nouvelles positions est", test.PositionX, test.PositionY)
		}
	}
}

func Test_creationvitesse(t *testing.T) { //vérif pour chaque particules si la vitesse est bien implémenté
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 10
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.Addvitesse = true
	A := NewSystem()
	A.Update()
	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i] //récupére la particule
		if test.SpeedX == 0 && test.SpeedY == 0 {
			t.Error(("Les vitesses ne sont pas correctement générées"))
		}
	}
}

func Test_vitesseavecrandomspawn(t *testing.T) {
	config.Get("../config.json")
	config.General.Artifice = false
	config.General.RandomSpawn = true
	config.General.SpawnRate = 0
	config.General.InitNumParticles = 10
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.Addvitesse = true
	A := NewSystem()
	var tabx []float64
	var taby []float64
	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i] //récupére la particule
		tabx = append(tabx, test.PositionX)
		taby = append(taby, test.PositionY)
	}
	A.Update()
	for i := 0; i < len(A.Content); i++ {
		var test Particle = A.Content[i] //récupére  la particule
		if tabx[i]+test.SpeedX != test.PositionX && taby[i]+test.SpeedY != test.PositionY {
			t.Error(("Les vitesses ne sont pas correctement générées"))
		}
	}
}

// ___________________________________________________________________________________________
// --------------------------------------Test de la partie 4.3--------------------------------
// ___________________________________________________________________________________________
func Test_aleg(t *testing.T) {
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.InitNumParticles = 0
	config.General.SpawnRate = 5
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.Addvitesse = false
	A := NewSystem()
	for i := 0; i < 150; i++ {
		A.Update()
		for i := 0; i < len(A.Content); i++ {

			var test Particle = A.Content[i] //récupére la particule

			if test.PositionX != 200 || test.PositionY != 300 {
				t.Error("la position est", test.PositionX, test.PositionY, "alors qu'elle devrait être", config.General.SpawnX, config.General.SpawnY)
			}
		}
	}

}
func Test_gvitesse(t *testing.T) {
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.SpawnRate = 10
	config.General.InitNumParticles = 0
	config.General.SpawnX = 200
	config.General.SpawnY = 300
	config.General.Addvitesse = true

	A := NewSystem()
	A.Update()

	for i := 0; i < len(A.Content); i++ {

		var test Particle = A.Content[i] //récupére la particule
		if test.PositionX != float64(config.General.SpawnX)+test.SpeedX && test.PositionY != float64(config.General.SpawnY)+test.SpeedY {
			t.Error("la position est", test.PositionX, test.PositionY, "", config.General.SpawnX, config.General.SpawnY)
		}
	}
}

func Test_nbrgparticule(t *testing.T) { //on vérifie que le nombre de particules creées correspond au spawnrate
	config.Get("../config.json")
	config.General.RandomSpawn = false
	config.General.InitNumParticles = 0
	config.General.SpawnRate = 1
	A := NewSystem()

	for i := 0; i < 10; i++ {
		A.Update()
	}
	if len(A.Content) != 10 {

		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", config.General.SpawnRate*10)
	}

	config.General.SpawnRate = 1256

	A = NewSystem()

	for i := 0; i < 10; i++ {
		A.Update()
	}
	if float64(len(A.Content)) != config.General.SpawnRate*10 {
		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", config.General.SpawnRate*10)
	}
	config.General.SpawnRate = 2
	A = NewSystem()

	for i := 0; i < 10; i++ {
		A.Update()
	}
	if float64(len(A.Content)) != config.General.SpawnRate*10 {
		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", config.General.SpawnRate*10)
	}

}

func Test_nbrgparticule2(t *testing.T) { //on vérifie que le nombre de particules creées correspond au spawnrate
	config.Get("../config.json")
	config.General.RandomSpawn = false

	config.General.InitNumParticles = 0

	config.General.SpawnRate = 0.5
	A := NewSystem()
	for i := 0; i < 10; i++ {
		A.Update()

	}
	if float64(len(A.Content)) != config.General.SpawnRate*10 {
		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", config.General.SpawnRate*10)
	}

	config.General.SpawnRate = 0.02
	A = NewSystem()

	for i := 0.0; i < 10000; i++ {
		A.Update()

	}
	if float64(len(A.Content)) != config.General.SpawnRate*10000 {
		t.Error("il y a", len(A.Content), "particules alors qu'ils devrait y en avoir", 10000)
	}

}
